function fetchData() {
    /// Replace 'your_api_endpoint' with the actual API endpoint you want to use
    const apiUrl = "https://api.adviceslip.com/advice";

    // Using the Fetch API to make a GET request
    fetch(apiUrl)
        .then((response) => response.json())
        .then((data) => {
        // Handle the data here
        console.log(data);

        // Display the data in the 'result' div
        document.getElementById("Advice").innerHTML = JSON.stringify(
            data.slip.advice,
            null,
            2
        );
        })
        .catch((error) => {
        // Handle errors here
        console.error("Error fetching data:", error);
        });
    }